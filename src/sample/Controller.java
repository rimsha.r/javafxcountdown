package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class Controller {

   @FXML
   public TextField inputField;

   @FXML
   public TextArea outputField;

   @FXML
   public void onButtonClick() {
       int input = 0;
       try {
           input = Integer.valueOf(String.valueOf(inputField.getText()));
       } catch (Exception a){
           outputField.setText("Please enter a valid number!");
       }

       if(input > 0){
           String output = "";
           for(int i = input; i >= 0; i--){
               output = output + i + "\n";
               outputField.setText(output);
           }

       } else if (input < 0) {
           String output = "";
           for(int i = input; i <= 0; i++){
               output = output + i + "\n";
               outputField.setText(output);
           }

       } else {
           outputField.setText("Please enter a valid number!");
       }
    }
}
